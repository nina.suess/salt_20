import 'jspsych'
import 'jspsych/plugins/jspsych-html-keyboard-response'

export default function get_fixcross(duration) {
    return {
        type: 'html-keyboard-response',
        stimulus: '<h1>+</h1>',
        choices: jsPsych.NO_KEYS,
        trial_duration: duration
    }
}