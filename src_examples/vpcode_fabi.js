/* jshint esversion: 6 */
import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import demographics from './assets/demographics/demographics.html';

export default function get_demographics() {
    return {
        type: 'survey-html-form',
        preamble: '<p style="text-align: left"><b style="font-size: 1.5rem;">Demographische Informationen</b></br></br>Dieses Online-Experiment ist Teil einer Studie, bei der Sie auch an einer MEG Messung in Salzburg teilnehmen können.'
            + ' Es ist sehr wichtig für uns, dass wir im Falle einer Teilnahme die Daten aus der MEG Messung mit den Ergebnissen dieses Experiments verknüpfen können. Dafür benötigen wir von Ihnen Informationen.'
            + ' Der Vor- und Mädchenname Ihrer Mutter dient hierbei zur Erstellung Ihres anonymisierten Versuchspersonencodes. '
            + ' Die Eingabe Ihrer E-mail Adresse ist für die Studienteilnahme zwar nicht verpflichtend, wird aber benötigt um Ihnen ihre Aufwandsentschädigung zukommen zu lassen.</br><br></p>',

        html: demographics,
        button_label: 'Weiter',
    };
}
