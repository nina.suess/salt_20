/**
 * jspsych-video-keyboard-response
 * Josh de Leeuw
 *
 * plugin for playing a video file and getting a keyboard response
 *
 * documentation: docs.jspsych.org
 *
 **/
import "jspsych";

jsPsych.plugins["video-text-response"] = (function() {

  var plugin = {};

  jsPsych.pluginAPI.registerPreload('video-keyboard-response', 'stimulus', 'video');

  plugin.info = {
    name: 'video-text-response',
    description: '',
    parameters: {
      sources: {
        type: jsPsych.plugins.parameterType.VIDEO,
        pretty_name: 'Video',
        default: undefined,
        description: 'The video file to play.'
      },
      prompt: {
        type: jsPsych.plugins.parameterType.STRING,
        pretty_name: 'Prompt',
        default: null,
        description: 'Any content here will be displayed below the stimulus.'
      },
      width: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Width',
        default: '',
        description: 'The width of the video in pixels.'
      },
      height: {
        type: jsPsych.plugins.parameterType.INT,
        pretty_name: 'Height',
        default: '',
        description: 'The height of the video display in pixels.'
      },
      autoplay: {
        type: jsPsych.plugins.parameterType.BOOL,
        pretty_name: 'Autoplay',
        default: true,
        description: 'If true, the video will begin playing as soon as it has loaded.'
      },
      controls: {
        type: jsPsych.plugins.parameterType.BOOL,
        pretty_name: 'Controls',
        default: false,
        description: 'If true, the subject will be able to pause the video or move the playback to any point in the video.'
      },

    }
  }

  plugin.trial = function(display_element, trial) {

    // setup stimulus
    var video_html = '<div>'
    video_html += '<video id="jspsych-video-keyboard-response-stimulus"';

    if(trial.width) {
      video_html += ' width="'+trial.width+'"';
    }
    if(trial.height) {
      video_html += ' height="'+trial.height+'"';
    }
    if(trial.autoplay){
      video_html += " autoplay ";
    }
    if(trial.controls){
      video_html +=" controls ";
    }
    video_html +=">";

    //variable to preload the video
    var video_preload_blob = jsPsych.pluginAPI.getVideoBuffer(trial.sources[0]);
    if(!video_preload_blob) {
      for(var i=0; i<trial.sources.length; i++){
        var file_name = trial.sources[i];
        if(file_name.indexOf('?') > -1){
          file_name = file_name.substring(0, file_name.indexOf('?'));
        }
        var type = file_name.substr(file_name.lastIndexOf('.') + 1);
        type = type.toLowerCase();
        video_html+='<source src="' + file_name + '" type="video/'+type+'">';
      }
    }
    video_html += "</video>";
    video_html += "</div>";

    // make play button
    const playbutton_html = `<div>
        <input id="jspsych-button-play" class="jspsych-btn jspsych-play" value="Play">
      </div>`;

    video_html += playbutton_html;

    // add prompt if there is one
    if (trial.prompt !== null) {
      video_html += trial.prompt;
    }

    const textinput_html = `<div>
      <input id="jspsych-video-text-response" class="jspsych-video-text" type="text">
    </div>`;

    // associative arrays
    //

    video_html += textinput_html;

    video_html += '<input type="submit" id="jspsych-button-submit" class="jspsych-btn jspsych-video-text" value="Weiter"></input>';

    display_element.innerHTML = video_html;

    //finish html

    // start preparing all the java script stuff

    if(video_preload_blob){
      display_element.querySelector('#jspsych-video-text-response-stimulus').src = video_preload_blob;
    }

  // make new variable to check if already played
    var counter = 0;

    // let it play here
    display_element.querySelector('#jspsych-button-play').onclick = (e) => {
      document.querySelector('#jspsych-video-keyboard-response-stimulus').play()
      console.log('Ok was clicked ' + counter++);

      // make it count to two and then disable the button
      if (counter == 2) {
        e.target.disabled = true;
      };

      };

    display_element.querySelector('#jspsych-button-submit').onclick = (e) => {
      var q_element = document.querySelector('#jspsych-video-text-response');
      var val = q_element.value;
      console.log(val)

      var trialdata = {
        "responses": JSON.stringify(val)
      };

      display_element.innerHTML = '';

      // next trial
      jsPsych.finishTrial(trialdata);
    };

  };

  return plugin;
})();
