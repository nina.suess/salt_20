/*
This Plugin can be used to find out which browser is used and if the browsertype is not in a list of supported browsers
the test subject will be informed that the study can't be continued unless one of the selected browsers is chosen.
Browsers are detected using ducktyping. 
*/
import Bowser from "bowser";
import "jspsych";

jsPsych.plugins['detect_browser'] = (function(){

    var plugin = {};

    plugin.info = {
        name: 'detect_browser',
        description: "",
        parameters: {
            platform: {
                type: jsPsych.plugins.parameterType.STRING,
                default: undefined,
                description: "The platforms that can be used. Can be mobile, desktop or tablet"
            },
            /*os: {
                type: jsPsych.plugins.parameterType.STRING,
                default: undefined,
                description: "The OS that can be used. Can be mac, linux or windows"
            },*/
            browser: {
                type: jsPsych.plugins.parameterType.STRING,
                default: undefined,
                description: "The Browsers that can be used"
            },
        }
    };

    plugin.trial = function(display_element, trial){

        // Get info about the browser, os and platform
        const browser = Bowser.parse(window.navigator.userAgent);


        if(trial.platform !== undefined && trial.platform.includes(browser.platform.type) === false){
            // Complain if platform is not as expected
            var platform;
            if(browser.platform.type === 'mobile'){
                platform = 'Smartphone';
            }
            if(browser.platform.type === 'desktop'){
                platform = 'Desktop-Computer oder Laptop';
            }
            if(browser.platform.type === 'tablet'){
                platform = 'Tablet';
            }
            display_element.innerHTML = "Dieses Experiment funktioniert nicht auf Ihrem " + platform + ". Versuchen Sie es nochmal auf einem Desktop-Computer oder Laptop."
        }
        /*else if(trial.os !== undefined && trial.os !== browser.os.name){
            // Complain if os is not as expected
            display_element.innerHTML = "Dieses Experiment funktioniert nicht auf Ihrem Betriebssystem. Versuchen Sie es nochmal mit " + trial.os
        }*/
        else if(trial.browser !== undefined && trial.browser.includes(browser.browser.name) === false){
            // Complain if browser is not as expected
            display_element.innerHTML = "Dieses Experiment funktioniert nicht auf " + browser.browser.name + ". Versuchen Sie es nochmal mit " + trial.browser.slice(0, -1) + " oder " + trial.browser[trial.browser.length - 1]
        }
        else {
            jsPsych.finishTrial();
        }
    };

    return plugin

})();



