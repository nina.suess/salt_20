import 'jspsych'
import 'jspsych/plugins/jspsych-html-button-response' 

import '../src_examples/parts/instructions'
import 'jspsych/plugins/jspsych-instructions';
import "jspsych/plugins/jspsych-html-button-response";

import get_browser from "./parts/browser";
import get_anfang from "./parts/anfang";
import get_anfang2 from "./parts/anfang2";
import get_demographics from "./parts/demographics";
import get_informed_consent from './parts/informed_consent';
import get_aphab from './parts/aphab' 
import get_numbers from './parts/numbers'
import get_words from './parts/words'
import get_sentences from './parts/sentences'
import get_ende from './parts/ende'

import get_text from './parts/test_text'
import get_num_videos from './parts/numbers_videos';
import get_words_videos from './parts/words_videos';
import get_sentences_videos from './parts/sentences_videos';

import './assets/css/salt20.css';

jsPsych.init({
    timeline: [
        get_browser(),
        get_anfang(),
        get_anfang2(),
        get_informed_consent(),
        get_demographics(),
        get_aphab(),
        get_numbers(),
        get_num_videos(),
        get_words(),
        get_words_videos(),
        get_sentences(),
        get_sentences_videos(),
        get_ende()
    ]
});