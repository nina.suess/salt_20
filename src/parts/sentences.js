import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import sentences from '../assets/html/sentences.html';

export default function get_sentences() {

    return {

        type: 'survey-html-form',

        html: sentences,
        button_label: 'Weiter',
    };

}
