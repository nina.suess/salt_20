import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';
import 'jspsych/plugins/jspsych-html-button-response';
import 'jspsych/plugins/jspsych-html-keyboard-response';
import 'jspsych/plugins/jspsych-survey-text';
import "../plugins/video_text_response"


import video1 from "../stimuli/numbers/z1_22_s2.mp4"
import video2 from "../stimuli/numbers/z1_33_s2.mp4"
import video3 from "../stimuli/numbers/z1_43_s2.mp4"
import video4 from "../stimuli/numbers/z1_45_s2.mp4"
import video5 from "../stimuli/numbers/z1_46_s2.mp4"
import video6 from "../stimuli/numbers/z1_47_s2.mp4"
import video7 from "../stimuli/numbers/z1_51_s2.mp4"
import video8 from "../stimuli/numbers/z1_54_s2.mp4"
import video9 from "../stimuli/numbers/z1_63_s2.mp4"
import video10 from "../stimuli/numbers/z1_71_s2.mp4"
import video11 from "../stimuli/numbers/z1_80_s2.mp4"
import video12 from "../stimuli/numbers/z1_86_s2.mp4"
import video13 from "../stimuli/numbers/z1_98_s2.mp4"
import video14 from "../stimuli/numbers/z1_99_s2.mp4"
import video15 from "../stimuli/numbers/z1_22_s3.mp4"
import video16 from "../stimuli/numbers/z1_33_s3.mp4"
import video17 from "../stimuli/numbers/z1_43_s3.mp4"
import video18 from "../stimuli/numbers/z1_45_s3.mp4"
import video19 from "../stimuli/numbers/z1_46_s3.mp4"
import video20 from "../stimuli/numbers/z1_47_s3.mp4"
import video21 from "../stimuli/numbers/z1_51_s3.mp4"
import video22 from "../stimuli/numbers/z1_54_s3.mp4"
import video23 from "../stimuli/numbers/z1_63_s3.mp4"
import video24 from "../stimuli/numbers/z1_71_s3.mp4"
import video25 from "../stimuli/numbers/z1_80_s3.mp4"
import video26 from "../stimuli/numbers/z1_86_s3.mp4"
import video27 from "../stimuli/numbers/z1_98_s3.mp4"
import video28 from "../stimuli/numbers/z1_99_s3.mp4"

const my_videos = {
    speaker1: {
        '22': video1,
        '33': video2,
        '43': video3,
        '45': video4,
        '46': video5,
        '47': video6,
        '51': video7,
        '54': video8,
        '63': video9,
        '71': video10,
        '80': video11,
        '86': video12,
        '98': video13,
        '99': video14,
    },
    speaker2: {
        '22': video15,
        '33': video16,
        '43': video17,
        '45': video18,
        '46': video19,
        '47': video20,
        '51': video21,
        '54': video22,
        '63': video23,
        '71': video24,
        '80': video25,
        '86': video26,
        '98': video27,
        '99': video28,
    }
}

// make randomized array here....
const all_answers = [22, 33, 43, 45, 46, 47, 51, 54, 63, 71, 80, 86, 98, 99];

// randomize all_answers
 function shuffleArray(array) {
     for (let i = array.length - 1; i > 0; i--) {
         const j = Math.floor(Math.random() * (i + 1));
         const temp = array[i];
         array[i] = array[j];
         array[j] = temp;
     }
 }

 shuffleArray(all_answers)

// pick 14  videos randomly
const final_numbers = [];

all_answers.forEach((val, idx) => {
    let cur_speaker = 'speaker1'
    if (idx >= all_answers.length/2) {
        cur_speaker = 'speaker2';
    }

    final_numbers.push(
        {
            video: [my_videos[cur_speaker][val]],
            cur_speaker: [cur_speaker],
            val: [val]
        }
    )
}
);

export default function get_num_videos() {
    "use strict";
    return {
        timeline: [
            {
                type: 'video-text-response',

                sources: jsPsych.timelineVariable('video'),
                choices: jsPsych.ALL_KEYS,
                prompt: 'Was wurde hier gesagt?',
                autoplay: false,
                controls: false,
                width: 1000,
            },
        ],
        timeline_variables: final_numbers,

        randomize_order: true
    }
}
