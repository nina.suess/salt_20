import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';
import 'jspsych/plugins/jspsych-html-button-response';
import 'jspsych/plugins/jspsych-html-keyboard-response';
import 'jspsych/plugins/jspsych-survey-text';


export default function get_text() {
    "use strict";
    return {
        type: 'survey-text',

        questions: [
            {prompt: "How old are you?"}, 
        ],

    }
}
