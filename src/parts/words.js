import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import words from '../assets/html/words.html';

export default function get_words() {

    return {

        type: 'survey-html-form',

        html: words,
        button_label: 'Weiter',
    };

}
