import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import anfang2 from '../assets/html/anfang2.html';


export default function get_anfang2() {

    return {

        type: 'survey-html-form',
        //preamble: '<p style="text-align: left"><u>Informationen für Teilnehmende der Studie</u><br><br></p>',

        html: anfang2,
        button_label: 'Weiter',
    };


}
