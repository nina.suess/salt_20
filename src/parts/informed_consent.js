import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import informed_consent from '../assets/html/informed_consent.html';


export default function get_informed_consent() {

    return {

        type: 'survey-html-form',
        //preamble: '<p style="text-align: left"><u>Einverständiserklärung</u><br><br></p>',

        html: informed_consent,
        button_label: 'Weiter',
    };
}
