import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';
import 'jspsych/plugins/jspsych-survey-likert';

export default function get_ssq() {
    "use strict";
    const options_q1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
    return {
        type: 'survey-likert',
        preamble: '<p style="text-align: justify"> <u>Sprache, räumliches Hören und Hörqualität (SSQ Deutsche Kurzform)</u><br><br>Die folgenden Fragen betreffen Ihre Erfahrungen und Fähigkeiten in verschiedenen ' +
        'Situationen zu hören und zu verstehen. Für jede Frage machen Sie bitte auf einer Skala von null bis zehn ein Kreuz (x). Wenn Sie eine <u>»10«</u> ankreuzen, bedeutet dies, dass Sie <u>perfekt</u> das machen ' +
        'können oder genau das erlebt haben, was in der Situation beschrieben ist. Wenn Sie eine <u>»0«</u> ankreuzen, bedeutet dies, dass Sie das in der Situation Geschilderte <u>nicht</u> machen können oder erlebt haben.<br><br>' +
            'Dazu folgendes Beispiel: In Frage eins wird danach gefragt, ob Sie mit einer Gruppe von fünf Personen in einem belebten Restaurant der Unterhaltung folgen können, wenn Sie jeden in der Gruppe sehen. ' +
            'Falls Sie der Unterhaltung gut folgen können, machen Sie bitte ein Kreuz auf der rechten Seite der Skala. Wenn Sie ungefähr die Hälfte der Unterhaltung verstehen können, machen Sie bitte ein Kreuz in der Mitte der Skala und so weiter.<br><br>' +
            'Wenn Sie Hörgeräte tragen, beantworten Sie die Fragen bitte so, wie Sie die jeweilige Situation mit Hörgeräten erleben.</p>',
            // speech understanding
        questions: [{prompt: "Sie sind mit einer Gruppe von etwa fünf Personen in einem belebten Restaurant\n" +
                "und Sie können jeden in der Gruppe sehen. Können Sie der Unterhaltung folgen?", labels: options_q1, required:true,},
            {prompt: "Sie sprechen mit einer anderen Person bei ständigen Nebengeräuschen, wie z.B.\n" +
                    "von einem surrenden Ventilator oder von fließendem Wasser. Können Sie die\n" +
                    "andere Person verstehen?", labels: options_q1, required: true, },
            {prompt: "Sie sprechen mit jemandem an einem Ort mit viel Nachhall/Echos, z.B. in einer\n" +
                    "Kirche oder Bahnhofshalle. Können Sie verstehen, was die andere Person sagt?", labels: options_q1, required:true, },
            {prompt: "Können Sie sich mit jemandem unterhalten während eine weitere Person in einer\n" +
                    "anderen Stimmlage spricht?", labels: options_q1, required:true, },
            {prompt: "Sie hören jemandem zu, der mit Ihnen spricht, während Sie gleichzeitig versuchen,\n" +
                    "der Nachrichtensendung im Fernsehen zu folgen. Können Sie beiden Personen\n" +
                    "folgen, was sie sagen?", labels: options_q1, required:true, },
            // spatial hearing
            {prompt: "Sie sind in einem Treppenhaus eines Gebäudes mit Stockwerken über und unter\n" +
                    "Ihnen. Sie hören Geräusche aus einem anderen Stockwerk. Können Sie problemlos sagen, woher das Geräusch kommt?", labels: options_q1, required: true, },
            {prompt: "Sie sind im Freien. Ein Hund bellt laut. Können Sie sofort sagen, wo der Hund ist,\n" +
                    "ohne hinzuschauen?", labels: options_q1, required:true, },
            {prompt: "Sie stehen auf dem Bürgersteig an einer belebten Strasse. Können Sie sofort\n" +
                    "hören, aus welcher Richtung ein Bus oder Lastwagen kommt, bevor Sie ihn sehen?", labels: options_q1, required:true, },
            {prompt: "Können Sie anhand des Geräusches sagen, wie weit ein Bus oder Lastwagen\n" +
                    "entfernt ist?", labels: options_q1, required:true, },
            // irrelevant
            {prompt: "Können Sie hören, wenn sich Ihnen jemand von hinten nähert?", labels: options_q1, required: true, },
            {prompt: "Können Sie anhand der Stimme oder den Schritten einer Person sagen, ob sie\n" +
                    "Ihnen entgegen kommt oder sich entfernt?", labels: options_q1, required:true, },
            // Hörqualität
            {prompt: "Sie sind in einem Raum und aus dem Radio kommt Musik. Es spricht eine andere\n" +
                    "Person in dem Raum. Können Sie die Stimme als getrennt von der Musik hören?", labels: options_q1, required:true, },
            {prompt: "Empfinden Sie es als einfach verschiedene Leute, die Sie kennen, anhand ihrer Stimme zu erkennen?", labels: options_q1, required:true, },
            {prompt: "Wenn Sie der Musik zuhören, klingt sie klar und natürlich?", labels: options_q1, required:true, },
            {prompt: "Klingen Alltagsgeräusche, die Sie leicht hören können, klar für Sie (nicht verschwommen)?", labels: options_q1, required:true, },
            {prompt: "Klingen die Stimmen anderer Menschen klar und natürlich für Sie?", labels: options_q1, required:true, },
            // listening_effort
            {prompt: "Sie sprechen mit einer anderen Person in einem ruhigen, mit Teppich ausgelegten Raum. Können Sie die andere Person verstehen?", labels: options_q1, required:true, },
            {prompt: 'Müssen Sie sich sehr anstrengen, um zu verstehen, was in einer Unterhaltung mit anderen gesagt wird? (10 bedeutet hier "Keine Anstrengung" 0 bedeutet "Sehr viel Anstrengung")', labels: options_q1, required:true, },
        ],
        button_label : 'Weiter',
 /*       on_load: function () {
            document.getElementById("jspsych-content").style.marginTop = "100px";
        },
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        }
        */
    };
}


