import "jspsych";
import "../plugins/browser_check/detect_browser"

export default function get_browser(){

    "use strict";

    return{
        timeline: [
            {
                type: 'detect_browser',
                platform: ['desktop'],
                //os: undefined,
                browser: ['Firefox', 'Chrome'],
            }
        ]
    };

}

