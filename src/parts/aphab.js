import 'jspsych';
import 'jspsych/plugins/jspsych-survey-multi-choice';
import 'jspsych/plugins/jspsych-survey-likert';

export default function get_aphab() {
    "use strict";
    const options_q1 = ['Immer', 'Fast immer', 'Häufig', 'In der Hälfte der Fälle', 'Gelegentlich', 'Selten', 'Nie'];
    return {
        type: 'survey-likert',
        preamble: '<p style="text-align: center"> <b>Subjektive Hörbeeinträchtigung</b><br><br>Die folgenden Fragen betreffen Ihre Erfahrungen und Fähigkeiten in verschiedenen Hörsituationen. ' +
        'Bitte wählen Sie die Anwort, die Ihrer alltäglichen Erfahrung am nächsten kommt. Wenn Sie eine bestimmte Situation nicht erlebt haben, stellen Sie vor, wie Sie in einer ähnlichen Situation antworten würden.<br><br></p>',
           
        // TO DO: wichtige informationen noch einfügen
        
        questions: [
            {prompt: "Wenn ich in einem belebten Lebensmittelgeschäft mit der Kassiererin spreche,\n" +
                "kann ich dem Gespräch folgen.", labels: options_q1, required:false, },
            {prompt: "Es entgeht mir viel Information, wenn ich mir einen Vortrag anhöre.", labels: options_q1, required: false, },
            {prompt: "Unerwartete Geräusche, wie einen Rauchmelder oder eine Alarmanlage, empfinde ich als unangenehm laut.", labels: options_q1, required:false, },
            {prompt: "Ich habe Schwierigkeiten, zu Hause einem Gespräch mit einem Familienangehörigen zu folgen.", labels: options_q1, required:false, },
            {prompt: "Ich habe Mühe, den Dialog in einem Film oder im Theater zu verstehen.", labels: options_q1, required:false, },
            {prompt: "Wenn ich am Autoradio die Nachrichten höre und sich Familienmitglieder dabei unterhalten, \n" +
                    "habe ich Mühe, die Nachrichten zu verstehen.", labels: options_q1, required: false, },
            {prompt: "Wenn ich mit mehreren Personen beim Essen sitze und ich mich mit einer Person unterhalten möchte,\n" +
                    "ist es für mich schwierig, diese zu verstehen.", labels: options_q1, required:false, },
            {prompt: "Verkehrslärm ist mir zu laut.", labels: options_q1, required:false, },
            {prompt: "Wenn ich mit jemanden spreche, der sich am anderen Ende eines großen leeren Raumes\n" +
                    "befindet, verstehe ich seine Worte.", labels: options_q1, required:false, },
            {prompt: "Wenn ich in einem kleinen Büroraum Fragen stelle oder beantworte, habe ich Schwierigkeiten,\n" +
                    "dem Gespräch zu folgen.", labels: options_q1, required: false, },
            {prompt: "Wenn ich im Kino oder Theater bin und die Leute um mich herum flüstern und mit Papier rascheln, \n" +
                    "kann ich dem Dialog immer noch folgen.", labels: options_q1, required:false, },
            {prompt: "Wenn ich mich mit einem Freund in einer ruhigen Umgebung unterhalte, habe ich Schwierigkeiten, ihn zu verstehen.", labels: options_q1, required:false, },
            {prompt: "Die Geräusche von fließendem Wasser, wie eine Toilettenspülung oder Dusche, sind mir unangenehm laut.", labels: options_q1, required:false, },
            {prompt: "Wenn ein Sprecher zu einer kleinen Gruppe spricht und alle ruhig zuhören, muss ich mich anstrengen, um ihn zu verstehen.", labels: options_q1, required:false, },
            {prompt: "Wenn ich mit meinem Arzt im Untersuchungszimmer spreche, fällt es mir schwer, dem Gespräch zu folgen.", labels: options_q1, required:false, },
            {prompt: "Ich kann einer Unterhaltung folgen, auch wenn mehrere Personen gleichzeitig sprechen.", labels: options_q1, required:false, },
            {prompt: "Baulärm ist mir unangenehm laut.", labels: options_q1, required:false, },
            {prompt: "Es ist für mich schwierig, zu verstehen, was bei Vorträgen oder in der Kirche gesprochen wird.", labels: options_q1, required:false, },
            {prompt: "Ich kann mich mit anderen unterhalten, wenn wir in einer Menschenmenge sind.", labels: options_q1, required:false, },
            {prompt: "Die Sirene eines nahen Feuerwehrfahrzeugs ist so laut, dass ich meine Ohren zuhalten muss.", labels: options_q1, required:false, },
            {prompt: "Im Gottesdienst kann ich die Worte der Predigt verstehen.", labels: options_q1, required:false, },
            {prompt: "Das Geräusch von quietschenden Bremsen ist mir unangenehm laut.", labels: options_q1, required:false, },
            {prompt: "Ich muss den Gesprächspartner bitten, sich zu wiederholen, wenn wir uns zu zweit in einem ruhigen Raum unterhalten.", labels: options_q1, required:false, },
            {prompt: "Ich habe Mühe, andere zu verstehen, wenn gleichzeitig eine Klimaanlage oder ein Ventilator läuft.", labels: options_q1, required:false, },
        ],
        button_label : 'Weiter',

        on_load: function () {
            document.getElementById("jspsych-content").style.textAlign = "center";
        },

        /*
        on_finish: function () {
            document.getElementById("jspsych-content").style.margin = "auto";
        }
        */
    };
}


