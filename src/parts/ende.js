import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import ende from '../assets/html/ende.html';

export default function get_ende() {

    return {

        type: 'survey-html-form',

        html: ende,
        button_label: 'Weiter',
    };

}
