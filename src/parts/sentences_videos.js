import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';
import 'jspsych/plugins/jspsych-html-button-response';
import 'jspsych/plugins/jspsych-html-keyboard-response';
import 'jspsych/plugins/jspsych-survey-text';
import "../plugins/video_text_response"

import video1 from "../stimuli/sentences/kl_63_s2.mp4"
import video2 from "../stimuli/sentences/kl_376_s2.mp4"
import video3 from "../stimuli/sentences/km_224_s2.mp4"
import video4 from "../stimuli/sentences/km_234_s2.mp4"
import video5 from "../stimuli/sentences/km_413_s2.mp4"
import video6 from "../stimuli/sentences/ks_119_s2.mp4"
import video7 from "../stimuli/sentences/ks_195_s2.mp4"
import video8 from "../stimuli/sentences/ll_352_s2.mp4"
import video9 from "../stimuli/sentences/ll_427_s2.mp4"
import video10 from "../stimuli/sentences/ll_437_s2.mp4"
import video11 from "../stimuli/sentences/ll_485_s2.mp4"
import video12 from "../stimuli/sentences/lm_29_s2.mp4"
import video13 from "../stimuli/sentences/lm_135_s2.mp4"
import video14 from "../stimuli/sentences/lm_229_s2.mp4"
import video15 from "../stimuli/sentences/ml_117_s2.mp4"
import video16 from "../stimuli/sentences/ml_38_s2.mp4"
import video17 from "../stimuli/sentences/ml_264_s2.mp4"
import video18 from "../stimuli/sentences/mm_49_s2.mp4"
import video19 from "../stimuli/sentences/mm_94_s2.mp4"
import video20 from "../stimuli/sentences/mm_114_s2.mp4"
import video21 from "../stimuli/sentences/ms_104_s2.mp4"
import video22 from "../stimuli/sentences/ms_572_s2.mp4"

import video23 from "../stimuli/sentences/kl_63_s3.mp4"
import video24 from "../stimuli/sentences/kl_376_s3.mp4"
import video25 from "../stimuli/sentences/km_224_s3.mp4"
import video26 from "../stimuli/sentences/km_234_s3.mp4"
import video27 from "../stimuli/sentences/km_413_s3.mp4"
import video28 from "../stimuli/sentences/ks_119_s3.mp4"
import video29 from "../stimuli/sentences/ks_195_s3.mp4"
import video30 from "../stimuli/sentences/ll_352_s3.mp4"
import video31 from "../stimuli/sentences/ll_427_s3.mp4"
import video32 from "../stimuli/sentences/ll_437_s3.mp4"
import video33 from "../stimuli/sentences/ll_485_s3.mp4"
import video34 from "../stimuli/sentences/lm_29_s3.mp4"
import video35 from "../stimuli/sentences/lm_135_s3.mp4"
import video36 from "../stimuli/sentences/lm_229_s3.mp4"
import video37 from "../stimuli/sentences/ml_117_s3.mp4"
import video38 from "../stimuli/sentences/ml_38_s3.mp4"
import video39 from "../stimuli/sentences/ml_264_s3.mp4"
import video40 from "../stimuli/sentences/mm_49_s3.mp4"
import video41 from "../stimuli/sentences/mm_94_s3.mp4"
import video42 from "../stimuli/sentences/mm_114_s3.mp4"
import video43 from "../stimuli/sentences/ms_104_s3.mp4"
import video44 from "../stimuli/sentences/ms_572_s3.mp4"

const my_videos = {
    speaker1: {
        'kl_63': video1,
        'kl_376': video2,
        'km_224': video3,
        'km_234': video4,
        'km_413': video5,
        'ks_119': video6,
        'ks_195': video7,
        'll_352': video8,
        'll_427': video9,
        'll_437': video10,
        'll_485': video11,
        'lm_29': video12,
        'lm_135': video13,
        'lm_229': video14,
        'ml_117': video15,
        'ml_38': video16,
        'ml_264': video17,
        'mm_49': video18,
        'mm_94': video19,
        'mm_114': video20,
        'ms_104': video21,
        'ms_572': video22,
    },
    speaker2: {
        'kl_63': video23,
        'kl_376': video24,
        'km_224': video25,
        'km_234': video26,
        'km_413': video27,
        'ks_119': video28,
        'ks_195': video29,
        'll_352': video30,
        'll_427': video31,
        'll_437': video32,
        'll_485': video33,
        'lm_29': video34,
        'lm_135': video35,
        'lm_229': video36,
        'ml_117': video37,
        'ml_38': video38,
        'ml_264': video39,
        'mm_49': video40,
        'mm_94': video41,
        'mm_114': video42,
        'ms_104': video43,
        'ms_572': video44,
    }
}

// make randomized array here....
const all_answers = ['kl_63', 'kl_376', 'km_224', 'km_234', 'km_413', 'ks_119', 'ks_195', 'll_352', 'll_427', 'll_437',
    'll_485', 'lm_29', 'lm_135', 'lm_229', 'ml_117', 'ml_38', 'ml_264', 'mm_49', 'mm_94', 'mm_114', 'ms_104', 'ms_572'];

// randomize all_answers
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

shuffleArray(all_answers)

// pick 28  videos randomly
const final_sentences = [];

all_answers.forEach((val, idx) => {
        let cur_speaker = 'speaker1'
        if (idx >= all_answers.length/2) {
            cur_speaker = 'speaker2';
        }

        final_sentences.push(
            {
                video: [my_videos[cur_speaker][val]],
                cur_speaker: [cur_speaker],
                val: [val]
            }
        )
    }
);

export default function get_sentences_videos() {
    "use strict";
    return {
        timeline: [
            {
                type: 'video-text-response',

                sources: jsPsych.timelineVariable('video'),
                choices: jsPsych.ALL_KEYS,
                prompt: 'Was wurde hier gesagt?',
                autoplay: false,
                controls: false,
                width: 1000,
            },
        ],
        timeline_variables: final_sentences,
        randomize_order: true
    }
}
