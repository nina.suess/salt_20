import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';
import 'jspsych/plugins/jspsych-html-button-response';
import 'jspsych/plugins/jspsych-html-keyboard-response';
import 'jspsych/plugins/jspsych-survey-text';
import "../plugins/video_text_response"


import video1 from "../stimuli/words/bl_8_s2_mann.mp4"
import video2 from "../stimuli/words/bl_11_s2_bild.mp4"
import video3 from "../stimuli/words/bl_33_s2_bier.mp4"
import video4 from "../stimuli/words/bl_47_s2_brot.mp4"
import video5 from "../stimuli/words/bm_69_s2_blatt.mp4"
import video6 from "../stimuli/words/bm_81_s2_bauch.mp4"
import video7 from "../stimuli/words/bm_82_s2_bank.mp4"
import video8 from "../stimuli/words/bm_87_s2_bach.mp4"
import video9 from "../stimuli/words/bm_89_s2_pech.mp4"
import video10 from "../stimuli/words/bm_96_s2_berg.mp4"
import video11 from "../stimuli/words/bs_339_s2_pflug.mp4"
import video12 from "../stimuli/words/bs_383_s2_pfahl.mp4"
import video13 from "../stimuli/words/nl_4_s2_weg.mp4"
import video14 from "../stimuli/words/nl_5_s2_frau.mp4"
import video15 from "../stimuli/words/nl_6_s2_fall.mp4"
import video16 from "../stimuli/words/nl_7_s2_gott.mp4"
import video17 from "../stimuli/words/nl_9_s2_geld.mp4"
import video18 from "../stimuli/words/nl_12_s2_uhr.mp4"
import video19 from "../stimuli/words/nm_68_s2_ei.mp4"
import video20 from "../stimuli/words/nm_70_s2_herz.mp4"
import video21 from "../stimuli/words/nm_71_s2_schatz.mp4"
import video22 from "../stimuli/words/nm_75_s2_licht.mp4"
import video23 from "../stimuli/words/nm_77_s2_ernst.mp4"
import video24 from "../stimuli/words/ns_333_s2_reif.mp4"
import video25 from "../stimuli/words/ns_334_s2_klee.mp4"
import video26 from "../stimuli/words/ns_335_s2_schmutz.mp4"
import video27 from "../stimuli/words/ns_337_s2_hecht.mp4"
import video28 from "../stimuli/words/ns_344_s2_kies.mp4"
import video29 from "../stimuli/words/bl_8_s3_mann.mp4"
import video30 from "../stimuli/words/bl_11_s3_bild.mp4"
import video31 from "../stimuli/words/bl_33_s3_bier.mp4"
import video32 from "../stimuli/words/bl_47_s3_brot.mp4"
import video33 from "../stimuli/words/bm_69_s3_blatt.mp4"
import video34 from "../stimuli/words/bm_81_s3_bauch.mp4"
import video35 from "../stimuli/words/bm_82_s3_bank.mp4"
import video36 from "../stimuli/words/bm_87_s3_bach.mp4"
import video37 from "../stimuli/words/bm_89_s3_pech.mp4"
import video38 from "../stimuli/words/bm_96_s3_berg.mp4"
import video39 from "../stimuli/words/bs_339_s3_pflug.mp4"
import video40 from "../stimuli/words/bs_383_s3_pfahl.mp4"
import video41 from "../stimuli/words/nl_4_s3_weg.mp4"
import video42 from "../stimuli/words/nl_5_s3_frau.mp4"
import video43 from "../stimuli/words/nl_6_s3_fall.mp4"
import video44 from "../stimuli/words/nl_7_s3_gott.mp4"
import video45 from "../stimuli/words/nl_9_s3_geld.mp4"
import video46 from "../stimuli/words/nl_12_s3_uhr.mp4"
import video47 from "../stimuli/words/nm_68_s3_ei.mp4"
import video48 from "../stimuli/words/nm_70_s3_herz.mp4"
import video49 from "../stimuli/words/nm_71_s3_schatz.mp4"
import video50 from "../stimuli/words/nm_75_s3_licht.mp4"
import video51 from "../stimuli/words/nm_77_s3_ernst.mp4"
import video52 from "../stimuli/words/ns_333_s3_reif.mp4"
import video53 from "../stimuli/words/ns_334_s3_klee.mp4"
import video54 from "../stimuli/words/ns_335_s3_schmutz.mp4"
import video55 from "../stimuli/words/ns_337_s3_hecht.mp4"
import video56 from "../stimuli/words/ns_344_s3_kies.mp4"

const my_videos = {
    speaker1: {
        'Mann': video1,
        'Bild': video2,
        'Bier': video3,
        'Brot': video4,
        'Blatt': video5,
        'Bauch': video6,
        'Bank': video7,
        'Bach': video8,
        'Pech': video9,
        'Berg': video10,
        'Pflug': video11,
        'Pfahl': video12,
        'Weg': video13,
        'Frau': video14,
        'Fall': video15,
        'Gott': video16,
        'Geld': video17,
        'Uhr': video18,
        'Ei': video19,
        'Herz': video20,
        'Schatz': video21,
        'Licht': video22,
        'Ernst': video23,
        'Reif': video24,
        'Klee': video25,
        'Schmutz': video26,
        'Hecht': video27,
        'Kies': video28
    },
    speaker2: {
        'Mann': video29,
        'Bild': video30,
        'Bier': video31,
        'Brot': video32,
        'Blatt': video33,
        'Bauch': video34,
        'Bank': video35,
        'Bach': video36,
        'Pech': video37,
        'Berg': video38,
        'Pflug': video39,
        'Pfahl': video40,
        'Weg': video41,
        'Frau': video42,
        'Fall': video43,
        'Gott': video44,
        'Geld': video45,
        'Uhr': video46,
        'Ei': video47,
        'Herz': video48,
        'Schatz': video49,
        'Licht': video50,
        'Ernst': video51,
        'Reif': video52,
        'Klee': video53,
        'Schmutz': video54,
        'Hecht': video55,
        'Kies': video56
    }
}

// make randomized array here....
const all_answers = ['Mann', 'Bild', 'Bier', 'Brot', 'Blatt', 'Bauch', 'Bank', 'Bach', 'Pech', 'Berg', 'Pflug',
    'Pfahl', 'Weg', 'Frau', 'Fall', 'Gott', 'Geld', 'Uhr', 'Ei', 'Herz', 'Schatz', 'Licht', 'Ernst', 'Reif', 'Klee', 'Schmutz', 'Hecht', 'Kies'];

// randomize all_answers
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

shuffleArray(all_answers)

// pick 28  videos randomly
const final_words = [];

all_answers.forEach((val, idx) => {
        let cur_speaker = 'speaker1'
        if (idx >= all_answers.length/2) {
            cur_speaker = 'speaker2';
        }

        final_words.push(
            {
                video: [my_videos[cur_speaker][val]],
                cur_speaker: [cur_speaker],
                val: [val]
            }
        )
    }
);

export default function get_words_videos() {
    "use strict";
    return {
        timeline: [
            {
                type: 'video-text-response',

                sources: jsPsych.timelineVariable('video'),
                choices: jsPsych.ALL_KEYS,
                prompt: 'Was wurde hier gesagt?',
                autoplay: false,
                controls: false,
                width: 1000,
            },
        ],
        timeline_variables: final_words,
        randomize_order: true
    }
}
