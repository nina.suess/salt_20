import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import anfang from '../assets/html/anfang.html';


export default function get_anfang() {

    return {

        type: 'survey-html-form',
        //preamble: '<p style="text-align: left"><u>Informationen für Teilnehmende der Studie</u><br><br></p>',

        html: anfang,
        button_label: 'Weiter',
    };


}
