import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import demographics from '../assets/html/demographics.html';


export default function get_demographics() {

    return {

        type: 'survey-html-form',
        //preamble: '<p style="text-align: left"><u>Demographische Daten</u><br><br></p>',

        html: demographics,
        button_label: 'Weiter',


    /*    on_load: function () {
            document.getElementById("jspsych-content").style.textAlign = "left";
      }, */

        on_finish: function () {
            document.getElementById("jspsych-content").style.textAlign = "auto";
        }

    };

 
}
