import 'jspsych';
import 'jspsych/plugins/jspsych-survey-html-form';

import numbers from '../assets/html/numbers.html';

export default function get_numbers() {

    return {

        type: 'survey-html-form',

        html: numbers,
        button_label: 'Weiter',
    };

}
